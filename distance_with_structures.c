//WAP to find the distance between two points using structures and 4 functions.

#include<stdio.h>
#include<math.h>
struct points
{
        float x, y;
};
float pointA(struct points a, struct points b)
{
       return (a.x - b.x)*(a.x - b.x);
}
float pointB(struct points a, struct points b)
{
      return (a.y - b.y)*(a.y - b.y);
}
float distance(struct points a, struct points b)
{
      return sqrt(pointA(a, b) + pointB(a, b));
}
float output(struct points a, struct points b)
{
      printf("%.2f\n",distance(a, b));
}
int main()
{
      struct points a,b;
      printf("Coordinates of Point A: ");
      scanf("%f%f",&a.x,&a.y);
      printf("Coordinates of Point B: ");
      scanf("%f%f",&b.x,&b.y);
      printf("Distance between A(%.2f, %.2f) and B(%.2f, %.2f) is: \n",a.x,a.y,b.x,b.y);
      output(a, b);
      return 0;
}
