//Write a program to add two user input numbers using one function.

#include <stdio.h>
int main()
{
   int num1, num2, num3;
   printf("Enter first and second number : ");
   scanf("%d %d", &num1,&num2);
   num3 = num1+num2;
   printf("Sum of the entered numbers: %d", num3);
   return 0;
}
