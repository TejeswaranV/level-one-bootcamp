//WAP to find the distance between two point using 4 functions.
#include<stdio.h>
#include<math.h>
float firstpoint(float x1, float x2)
{
  return x2 - x1;
}
float secondpoint(float y1, float y2)
{
  return y2 - y1;
}
float distance(float x1, float x2, float y1, float y2)
{
  return sqrt(firstpoint(x1,x2)*firstpoint(x1, x2)+secondpoint(y1, y2)*secondpoint(y1, y2));
}
int main()
{
  float x1, x2, y1, y2;
  printf("enter coordinates of firstpoint (x1,y1) :\n");
  scanf("%f%f",&x1, &y1);
  printf("enter coordinates of secondpoint (x2, y2):\n");
  scanf("%f%f", &x2, &y2);
  printf("Distance between (%.2f, %.2f) and (%.2f, %.2f) is %.2f",x1, y1, x2, y2, distance(x1,x2,y1,y2));
  return 0;
}